﻿using OfficeOpenXml;
using PuppeteerSharp;
using System.Text.RegularExpressions;
using VinCodeGeneratorNew.Helpers;
using VinCodeGeneratorNew.Models;

namespace VinCodeGeneratorNew
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            await GenerateCodes();
            //Random r = new Random();

            //List<ExcelData> data = new List<ExcelData>();

            //for (int i = 1; i < 301; i++)
            //{
            //    data.Add(new ExcelData
            //    {
            //        Id = i,
            //        Code = r.Next(1000, 9999).ToString()
            //    });
            //}

            //await GenerateExcel(data);
            //Environment.Exit(0);
        }
        #region Generate Excel Document
        private static Task GenerateExcel(List<ExcelData> excelDataList)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");

                for (int i = 1; i <= 5; i++)
                {
                    worksheet.Column(i).Width = 15; 
                }

                int row = 1;
                int col = 1;

                foreach (var item in excelDataList)
                {
                    worksheet.Cells[row, col].Value = $"Code {item.Id}";

                    worksheet.Cells[row + 1, col].Value = item.Code;

                    col++;
                    if (col > 5)
                    {
                        row += 3;

                        col = 1;
                    }
                    else
                    {
                        worksheet.Cells[row + 1, col].Value = string.Empty;
                    }

                    //if (row > 10)
                    //    break;
                }

                FileInfo fileInfo = new FileInfo($"output-{((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds()}.xlsx");
                package.SaveAs(fileInfo);
            }

            Console.WriteLine("Excel file created successfully.");

            return Task.CompletedTask;
        }
        #endregion

        #region Generate Codes
        private static async Task GenerateCodes()
        {
            using (var codeGeneratorHelper = new CodeGeneratorHelper())
            {
                using (var browserFetcher = new BrowserFetcher())
                {
                    Console.WriteLine("Downloading browser...");
                    await browserFetcher.DownloadAsync();

                    using var browser = await Puppeteer.LaunchAsync(new LaunchOptions
                    {
                        Headless = false,
                        Browser = SupportedBrowser.Chrome,
                        Timeout = int.MaxValue,
                        Args = new[] { "--start-maximized" }
                    });
                    var page = await browser.NewPageAsync();
                    page.DefaultTimeout = int.MaxValue;

                    try
                    {
                        await page.GoToAsync(codeGeneratorHelper.Url);

                        Console.WriteLine("Web page has been loaded.");

                        SelectPhoneNumber: Console.WriteLine("Please select a phone number in which the OTP will be sent to: ");

                        for (int k = 0; k < codeGeneratorHelper.AdminInfos!.Count; k++)
                        {
                            Console.WriteLine($"{k + 1}. {codeGeneratorHelper.AdminInfos[k].Name}: {codeGeneratorHelper.AdminInfos[k].PhoneNumber}");
                        }

                        string? adminInfoNumber = Console.ReadLine();

                        if (int.TryParse(adminInfoNumber, out int adminNumber) && adminNumber > 0 && adminNumber <= codeGeneratorHelper.AdminInfos.Count)
                        {
                            await page.WaitForTimeoutAsync(codeGeneratorHelper.SmallDelay);

                            await page.WaitForSelectorAsync(codeGeneratorHelper.SingleInputSelector);
                            await page.TypeAsync(codeGeneratorHelper.SingleInputSelector, codeGeneratorHelper.AdminInfos[adminNumber - 1].PhoneNumber);

                            await page.WaitForSelectorAsync((codeGeneratorHelper.SendOTPSelector));
                            await page.ClickAsync((codeGeneratorHelper.SendOTPSelector));

                            Console.Write("Type '1' when you have successfully logged in with the OTP: ");
                            string? message = Console.ReadLine();

                            if (message == "1")
                            {
                                Console.WriteLine(codeGeneratorHelper.PageBreak);
                                StartNumber: Console.Write("What is the starting number? ");
                                string? startingNumber = Console.ReadLine();

                                if (int.TryParse(startingNumber, out int index))
                                {
                                    CodesToGenerate: Console.Write("How many codes do you want to generate? ");
                                    string? codesToGenerate = Console.ReadLine();

                                    Console.WriteLine(codeGeneratorHelper.PageBreak);

                                    if (int.TryParse(codesToGenerate, out int max))
                                    {
                                        for (int i = index; i < (max + index); i++)
                                        {
                                            #region Create new code click
                                            await page.WaitForSelectorAsync(codeGeneratorHelper.AddNewCodeSelector);
                                            await page.ClickAsync(codeGeneratorHelper.AddNewCodeSelector);
                                            #endregion

                                            await page.WaitForTimeoutAsync(codeGeneratorHelper.SmallDelay);

                                            #region Name input
                                            await page.WaitForSelectorAsync(codeGeneratorHelper.NameSelector);
                                            await page.TypeAsync(codeGeneratorHelper.NameSelector, "");
                                            await page.TypeAsync(codeGeneratorHelper.NameSelector, $"{i}");
                                            #endregion

                                            #region Cellphone number input
                                            await page.WaitForSelectorAsync(codeGeneratorHelper.CellNumberSelector);
                                            await page.TypeAsync(codeGeneratorHelper.CellNumberSelector, "");
                                            await page.TypeAsync(codeGeneratorHelper.CellNumberSelector, GetCellPhoneNumber($"{i}"));
                                            #endregion

                                            await page.WaitForTimeoutAsync(codeGeneratorHelper.SmallDelay);

                                            #region Click save
                                            await page.WaitForSelectorAsync(codeGeneratorHelper.SaveCodeSelector);
                                            await page.ClickAsync(codeGeneratorHelper.SaveCodeSelector);
                                            #endregion

                                            Console.WriteLine($"Code generated for: {i}");

                                            GetGeneratedCode: await page.WaitForTimeoutAsync(codeGeneratorHelper.SmallDelay);

                                            try
                                            {
                                                #region Get the generated code
                                                await page.WaitForSelectorAsync(codeGeneratorHelper.ParentSelector);
                                                var parentElement = await page.QuerySelectorAsync(codeGeneratorHelper.ParentSelector);

                                                if (parentElement != null)
                                                {
                                                    await parentElement.WaitForSelectorAsync(codeGeneratorHelper.ChildSelector);
                                                    var firstTableRow = await parentElement.QuerySelectorAsync(codeGeneratorHelper.ChildSelector);

                                                    if (firstTableRow != null)
                                                    {
                                                        // Click on the found div
                                                        await firstTableRow.ClickAsync();

                                                        await page.WaitForTimeoutAsync(codeGeneratorHelper.SmallDelay);

                                                        // Wait for the value div to be present
                                                        await page.WaitForSelectorAsync(codeGeneratorHelper.ValueSelector);

                                                        // Get the text content of the value div
                                                        var valueElement = await page.QuerySelectorAsync(codeGeneratorHelper.ValueSelector);
                                                        string value = await valueElement.EvaluateFunctionAsync<string>(codeGeneratorHelper.NodeTextContentScript);

                                                        string codeValue = GetNumberFromCode(value);

                                                        codeGeneratorHelper.ExcelDatas!.Add(new ExcelData
                                                        {
                                                            Id = i,
                                                            Code = codeValue
                                                        });

                                                        #region Close code
                                                        await page.WaitForSelectorAsync(codeGeneratorHelper.CloseCodeSelector);
                                                        await page.ClickAsync(codeGeneratorHelper.CloseCodeSelector);
                                                        #endregion

                                                        await page.WaitForTimeoutAsync(codeGeneratorHelper.SmallDelay);
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine($"No matching {codeGeneratorHelper.ChildSelector} element found inside {codeGeneratorHelper.ParentSelector}");
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine($"No matching {codeGeneratorHelper.ParentSelector} element found");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Console.WriteLine("Error: " + ex.Message);
                                                goto GetGeneratedCode;
                                            }
                                            #endregion

                                            await page.WaitForTimeoutAsync(codeGeneratorHelper.BigDelay);
                                        }
                                        #region Excel 
                                        Console.WriteLine(codeGeneratorHelper.PageBreak);
                                        await GenerateExcel(codeGeneratorHelper.ExcelDatas!);
                                        #endregion
                                    }
                                    else
                                    {
                                        Console.WriteLine("The value entered is not a number.");
                                        goto CodesToGenerate;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("The value entered is not a number.");
                                    goto StartNumber;
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("User does not exist!");
                            goto SelectPhoneNumber;
                        }
                        Console.WriteLine("All codes have been generated.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                    finally
                    {
                        await page.CloseAsync();
                    }
                }
            }
        }
        #endregion

        #region Helper Methods
        private static string GetNumberFromCode(string inputString)
        {
            string pattern = @"\d+";

            Match match = Regex.Match(inputString, pattern);

            if (match.Success)
            {
                string number = match.Value;
                Console.WriteLine($"Extracted number: {number}");
                return number;
            }
            else
            {
                Console.WriteLine("No number found in the input string");
                return "invalid";
            }
        }

        private static string GetCellPhoneNumber(string i)
        {
            int zeroesToCreate = 10 - i.Length;

            string returnString = string.Empty;

            for (int j = 0; j < (zeroesToCreate + 1); j++)
            {
                returnString += "0";
            }

            return returnString += i;
        }
        #endregion
    }
}