﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinCodeGeneratorNew.Models
{
    public class AdminInfo
    {
        public string? Name { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
