﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinCodeGeneratorNew.Models
{
    public class ExcelData
    {
        public int? Id { get; set; }
        public string? Code { get; set; }
    }
}
