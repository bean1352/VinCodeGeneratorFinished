﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinCodeGeneratorNew.Models;

namespace VinCodeGeneratorNew.Helpers
{
    public class CodeGeneratorHelper: IDisposable
    {
        #region Properties
        public string? Url { get; set; }
        public string? SingleInputSelector { get; set; }
        public string? PageBreak { get; set; }
        public string? SendOTPSelector { get; set; }
        public string? AddNewCodeSelector { get; set; }
        public string? NameSelector { get; set; }
        public string? CellNumberSelector { get; set; }
        public string? SaveCodeSelector { get; set; }
        public int BigDelay { get; set; }
        public int SmallDelay { get; set; }
        public List<AdminInfo>? AdminInfos { get; set; }
        public List<ExcelData>? ExcelDatas { get; set; }
        public string? ParentSelector { get; set; }
        public string? ChildSelector { get; set; }
        public string? ValueSelector { get; set; }
        public string? NodeTextContentScript { get; set; }
        public string? CloseCodeSelector { get; set; }
        #endregion

        #region Constructor
        public CodeGeneratorHelper()
        {
            InstantiateProperties();
        }
        #endregion

        #region Helper Methods
        private void InstantiateProperties()
        {
            Url = "https://atthegate.biz/userapp/?page=longlakeedge%40atgdigital.biz&sub=visitors-list";
            SingleInputSelector = "[is= 'iron-input']";
            PageBreak = "-----------------------------------------------";
            SendOTPSelector = ".style-scope paper-button";
            AddNewCodeSelector = ".add-button.style-scope.notif-visitor-profiles.x-scope.paper-icon-button-0";
            NameSelector = "input[aria-labelledby='paper-input-label-17']";
            CellNumberSelector = "input[aria-labelledby='paper-input-label-24']";
            SaveCodeSelector = "#dialogVisitor > div.controls.style-scope.notif-visitor-profile-popup > div.buttons-container.align-right.style-scope.notif-visitor-profile-popup > paper-button";
            BigDelay = 5000;
            SmallDelay = 2000;
            AdminInfos = new List<AdminInfo>()
            {
                new AdminInfo()
                {
                    Name = "Nina",
                    PhoneNumber = "0638225679"
                },
                new AdminInfo()
                {
                    Name = "Jarrod",
                    PhoneNumber = "0812802833"
                }
            };
            ExcelDatas = new List<ExcelData>();
            ParentSelector = "notif-visitor-profiles-tr.style-scope.notif-visitor-profiles-tr-0";
            ChildSelector = "div.table-row.style-scope.notif-visitor-profiles-tr";
            ValueSelector = "div.detail-value.margin-left.center.flex-horizontal.style-scope.notif-visitor-profile-popup";
            NodeTextContentScript = "node => node.textContent.trim()";
            CloseCodeSelector = "#dialogVisitor > div.controls.style-scope.notif-visitor-profile-popup > div.buttons-container.align-right.style-scope.notif-visitor-profile-popup > paper-button:nth-child(5)";
        }
        #endregion

        #region Dispose
        public void Dispose()
        {

        }
        #endregion
    }
}
